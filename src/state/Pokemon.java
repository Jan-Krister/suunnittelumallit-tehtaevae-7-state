package state;

public abstract class Pokemon {

	void changeState(Creature creature, Pokemon state) {
		creature.changeState(state);
	}

	protected int powerLevel = 0;

	public int getPowerLevel() {
		return powerLevel;
	}

	public void setPowerLevel(int powerLevel) {
		this.powerLevel = powerLevel;
	}

	public abstract void eat();

	public abstract void sleep();

	public abstract void exercise();

	public abstract void evolve(Creature creature);

}
