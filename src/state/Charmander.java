package state;

public class Charmander extends Pokemon {

	private static Charmander INSTANCE = new Charmander();

	private Charmander() {

	}

	public static Charmander getState() {
		return INSTANCE;
	}

	public void exercise() {
		setPowerLevel(getPowerLevel() + 100);
		System.out.println("Charmander has started to exercise and increases his powerlevel to: " + getPowerLevel());
	}

	public void eat() {
		setPowerLevel(getPowerLevel() + 50);
		System.out.println("Charmander eats porridge increasing his powerlevel to: " + getPowerLevel());
	}

	public void sleep() {
		setPowerLevel(getPowerLevel() + 80);
		System.out.println("Charmander sleeps increasing his powerlevel to: " + getPowerLevel());
	}

	@Override
	public void evolve(Creature creature) {
		if (getPowerLevel() >= 250) {
			System.out.println("Charmander evolves to Charmeleon");
			changeState(creature, Charmeleon.getState());
		} else {
			System.out.println("Charmander is a pussy and needs to get his powerlevel up by " + (250 - getPowerLevel())
					+ " in order to level.");
		}
	}

}