package state;

public class Charmeleon extends Pokemon {

	private static Charmeleon INSTANCE = new Charmeleon();

	private Charmeleon() {

	}

	public static Charmeleon getState() {
		return INSTANCE;
	}

	public void exercise() {
		setPowerLevel(getPowerLevel() + 125);
		System.out.println(
				"Charmeleon has started to exercise with weights and increases his powerlevel to: " + getPowerLevel());
	}

	public void eat() {
		setPowerLevel(getPowerLevel() + 60);
		System.out.println("Charmeleon eats fish increasing his powerlevel to: " + getPowerLevel());
	}

	public void sleep() {
		setPowerLevel(getPowerLevel() + 95);
		System.out.println("Charmeleon takes a power nap increasing his powerlevel to: " + getPowerLevel());
	}

	@Override
	public void evolve(Creature creature) {
		if (getPowerLevel() >= 500) {
			System.out.println("Charmeleon has evolved to Charizard");
			changeState(creature, Charizard.getState());
		} else {
			int remaining = 500 - getPowerLevel();
			System.out.println("Charmeleon failed to evolve and needs to get his powerlevel up by: " + remaining
					+ " in order to evolve");
		}
	}

}
