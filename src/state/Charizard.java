package state;

public class Charizard extends Pokemon {

	private static Charizard INSTANCE = new Charizard();

	private Charizard() {

	}

	public static Charizard getState() {
		return INSTANCE;
	}

	public void exercise() {
		setPowerLevel(getPowerLevel() + 100);
		System.out.println("Charizard has started to exercise and increases his powerlevel to: " + getPowerLevel());
	}

	public void eat() {
		setPowerLevel(getPowerLevel() + 50);
		System.out.println("Charizard eats increasing his powerlevel to: " + getPowerLevel());
	}

	public void sleep() {
		setPowerLevel(getPowerLevel() + 75);
		System.out.println("Charizard sleeps increasing his powerlevel to: " + getPowerLevel());
	}

	@Override
	public void evolve(Creature creature) {
		System.out.println("Charizard is now complete");
		creature.setRunning(false);
	}

}
