package state;

import java.util.concurrent.ThreadLocalRandom;

public class Creature implements PokemonInterface {
	private Pokemon state;
	private boolean running = true;
	int min = 1;
	int max = 4;

	public void setRunning(boolean running) {
		this.running = running;
	}

	public Creature() {
		this.state = Charmander.getState();

	}

	protected void changeState(Pokemon state) {
		this.state = state;
	}

	public void startEvolving() {
		while (running) {
			try {
				Thread.sleep(750);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			int rand = ThreadLocalRandom.current().nextInt(min, max + 1);
			switch (rand) {
			case 1: {
				state.eat();
				break;
			}
			case 2: {
				state.exercise();
				break;
			}
			case 3: {
				state.sleep();
				break;
			}
			case 4: {
				state.evolve(this);
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + rand);
			}
		}
	}

}
